def get_surface(length, width, height):
    side_1 = length * width
    extra = side_1

    side_2 = width * height
    if side_2 < extra:
        extra = side_2

    side_3 = height * length
    if side_3 < extra:
        extra = side_3

    surface = 2 * side_1 + 2 * side_2 + 2 * side_3
    return surface + extra


def get_ribbon_feet(length, width, height):
    values = [length, width, height]
    values.sort()
    values.pop()

    present_ribbon_feet = values[0] * 2 + values[1] * 2
    bow_ribbon_feet = length * width * height

    return present_ribbon_feet + bow_ribbon_feet

input = open('input-day-2.txt', 'r')
dimensions = input.readlines()

total_square_feet = 0
total_ribbon_feet = 0

for dimension in dimensions:
    length, width, height = dimension.rstrip().split('x')

    total_square_feet += get_surface(int(length), int(width), int(height))
    total_ribbon_feet += get_ribbon_feet(int(length), int(width), int(height))

print(total_square_feet)
print(total_ribbon_feet)
