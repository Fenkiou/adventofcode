input = open('input-day-1.txt', 'r')

STEPS = input.readline()

FLOOR_UP = '('
FLOOR_DOWN = ')'

current_floor = 0
position = 0

for index, step in enumerate(STEPS):
    if step == FLOOR_UP:
        current_floor += 1
    else:
        current_floor -= 1

    position += 1
    if current_floor == -1:
        print("In basement at position : {}".format(position))

print("Floor destination : {}".format(current_floor))
