input = open('input-day-3.txt', 'r')

steps = input.readline()
steps = '^>v<'
# steps = '^v^v^v^v^v'
santa_steps, robo_santa_steps = steps[::2], steps[1::2]

NORTH = '^'
SOUTH = 'v'
EAST = '>'
WEST = '<'


def houses_delivred(steps):
    houses_nb = 1
    line = 0
    column = 0
    matrix = [[line, column]]

    for step in steps:
        if step == NORTH:
            line += 1
        elif step == SOUTH:
            line -= 1
        elif step == EAST:
            column += 1
        elif step == WEST:
            column -= 1
        else:
            continue

        position = [line, column]

        if position not in matrix:
            houses_nb += 1

        matrix.append(position)

    return houses_nb

houses_nb = houses_delivred(steps)
print(houses_nb)

houses_nb = houses_delivred(santa_steps)
houses_nb += houses_delivred(robo_santa_steps) - 1
print(houses_nb)
